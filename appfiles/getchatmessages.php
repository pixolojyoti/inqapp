<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$teacherid = $_GET['teacherid'];
$studentid = $_GET['studentid'];
$userid = $_GET['userid'];

	$stmt = $mysqli->prepare ( "SELECT messageid, studentid, teacherid, message, image, senderid, is_teacherread, is_studentread, createdDate FROM chatmessage WHERE teacherid = '$teacherid' AND studentid = '$studentid' ORDER BY messageid ASC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $messageid, $studentid, $teacherid, $message, $image, $senderid, $is_teacherread, $is_studentread, $createdDate );
		$stmt->store_result ();
		$messagesdata = array();
		while($row = $stmt->fetch ()) {
			$messagesdata[] = array(
				'messageid' => $messageid,
				'studentid' => $studentid,
				'teacherid' => $teacherid,
				'message' => $message,
				'image' => $image,
				'senderid' => $senderid,
				'is_teacherread' => $is_teacherread,
				'is_studentread' => $is_studentread,
				'createdDate' => date('h:i a',strtotime($createdDate))
			);
		}
		
		$userdata = select($mysqli, "users", "userid = '$userid'", "1");
		$usertype = $userdata['userstype'];
		if($usertype==1){
			$update = update($mysqli, "chatmessage", "is_teacherread = '1'", "teacherid = '$teacherid' AND studentid = '$studentid'");	
			header('Content-type: application/json');
			echo json_encode($messagesdata);
		}
		else{
			$update = update($mysqli, "chatmessage", "is_studentread = '1'", "teacherid = '$teacherid' AND studentid = '$studentid'");	
			header('Content-type: application/json');
			echo json_encode($messagesdata);
		}

	}
	else
	{
		echo "0";

	}

?>
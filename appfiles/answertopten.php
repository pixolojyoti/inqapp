<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$standardid = $_GET['standardid'];
	$stmta = $mysqli->prepare("SELECT todate, fromdate FROM leaderboard ORDER BY lboardid DESC LIMIT 1;");
	$stmta->execute ();
	$stmta->bind_result ( $todate, $fromdate );
	$stmta->store_result ();
	$stmta->fetch ();

	
	$stmt = $mysqli->prepare ( "SELECT COUNT(answers.answerid) as acount, answers.userid FROM answers INNER JOIN users ON answers.userid = users.userid WHERE users.standard = '$standardid' AND answers.verified = '1' AND answers.ausertype > 1 AND answers.createdDate >= '$fromdate' AND answers.createdDate <=  '$todate' GROUP BY answers.userid ORDER BY acount DESC LIMIT 10");
	if($stmt->execute())
	{
		$stmt->bind_result ( $acount, $userid );
		$stmt->store_result ();
		$achartdata = array();
		while($row = $stmt->fetch ()) {
			
			$userdata = select($mysqli, "users", "userid = '$userid'", "1");
			$ausername = $userdata['name'];
			$ausertype = $userdata['userstype'];
			
			$achartdata[] = array(
				'acount' => $acount,
				'ausertype' => $ausertype,
				'ausername' => $ausername
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($achartdata);

	}
	else
	{
		echo "0";

	}

?>
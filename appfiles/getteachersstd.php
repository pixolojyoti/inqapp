<?php
require_once("connect.php");

$userid = $_POST['id'];

	$stmt = $mysqli->prepare ( "SELECT standard.standardid, standard.standard, users.name FROM users INNER JOIN standard ON users.standard = standard.standardid WHERE users.userid = ?" );
	$stmt->bind_param ( "i", $userid );
	if($stmt->execute())
	{
		$stmt->bind_result ( $standardid, $standard, $name );
		$stmt->store_result ();
		$row = $stmt->fetch ();
		$std = $standard." th";
		$teacherdata = array();
		$teacherdata[] = array(
			'standardid' => $standardid,
			'standard' => $standard
		);
		header('Content-type: application/json');
		echo json_encode(array('teacherdata'=>$teacherdata));

	}
	else
	{
		echo "0";

	}

?>
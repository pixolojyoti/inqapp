<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$standardid = $_GET['standardid'];

	$stmt = $mysqli->prepare ( "SELECT subjectid, subject, iconpath, inacticonpath FROM subject WHERE standardid = ?" );
	$stmt->bind_param ( 'i', $standardid );
	if($stmt->execute())
	{
		$stmt->bind_result ( $subjectid, $subject, $iconpath, $inacticonpath );
		$stmt->store_result ();
		$subjectdata = array();
		while($row = $stmt->fetch ()) {
			$chapterdata = array();
			$path1 = "http://inqapp.crextal.com/admin/".$iconpath;
			$path2 = "http://inqapp.crextal.com/admin/".$inacticonpath;
			
			$stmti = $mysqli->prepare("SELECT chapterid, chaptertitle FROM chapter WHERE subjectid = ? ;");
			$stmti->bind_param ( 'i', $subjectid );
			$stmti->execute();
			$stmti->bind_result ( $chapterid, $chaptertitle );
			$stmti->store_result ();
			while($rowi = $stmti->fetch ()){
			$chapterdata[] = array(
				'chapterid' => $chapterid,
				'chaptertitle' => $chaptertitle
				);
			}
			$subjectdata[] = array(
				'subjectid' => $subjectid,
				'subject' => $subject,
				'iconpath' => $path1,
				'inacticonpath' => $path2,
				'chapters' => $chapterdata
			);
		}
		
		header('Content-type: application/json');
		echo json_encode($subjectdata);

	}
	else
	{
		echo "0";

	}

?>
<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$rname = trim($_GET['name']);
$rcontact = trim($_GET['contact']);
$remail = trim($_GET['email']);
$rpassword = trim($_GET['password']);
$rstandard = $_GET['standard'];
$rboard = $_GET['board'];
$rschool = $_GET['school'];
$ruserstype = "3";
//$ruserappid = "IN".time();
$stmt = $mysqli->prepare ( "SELECT contact as ctct FROM users WHERE contact = ?" );
$stmt->bind_param ( "s", $rcontact );
$stmt->execute();
$stmt->bind_result ( $ctct );
$stmt->store_result ();
$stmt->fetch ();
if($stmt->num_rows > 0 ){
	echo "0";
} else {
	$subject = "Login Credentails for INQ App";
	$header = "From: noreply@learnwithinq.com\r\n";
	$header .= "Reply-To: sunalisclasses@gmail.com\r\n";
	$header .= "Content-type: text/html\r\n";
	$message = "Hello <b>".ucwords($rname)."</b>, <br/><br/>";
	$message.= "You have successfully registered with <b>Sunali’s Classes App</b>. You can access your Student profile with the following credentials:<br/><br/>";
	$message.= "Username : <b>$rcontact</b><br/>";
	$message.= "Password :  <b>$rpassword</b><br/><br/>";
	$message.= "Feel free to drop us your feedback on  Playstore: <a href='https://play.google.com/store/apps/details?id=com.ideamagix.inq'>Click here.</a> <br/>";
	$message.= "For queries mail us at <a href='mailto:sunalisclasses@gmail.com'>sunalisclasses@gmail.com<a> or call us at <b>9427711412</b> \n\r";
	
	$stmti = $mysqli->prepare("INSERT INTO users (userstype, name, email, password, contact, standard, board, school, createdDate ) VALUES (?,?,?,?,?,?,?,?,?)");
	$stmti->bind_param ( "issssiiss", $ruserstype, $rname, $remail, $rpassword, $rcontact, $rstandard, $rboard, $rschool, $date );
	$stmti->execute ();
	$last_id = $stmti->insert_id;
	if(mail($remail, $subject, $message, $header))
	{
		$upmail = update($mysqli, "users", "mailsend = '1'", "userid = $last_id");
		
	}
	$stmtu = $mysqli->prepare ( "SELECT userid, userstype, userappid, name, email, password, contact, standard, board, school, status, createdDate, updatedDate FROM users WHERE userid = '$last_id'" );
	$stmtu->execute();
	$stmtu->bind_result ( $userid, $userstype, $userappid, $name, $email, $password, $contact, $standardid, $boardid, $school, $status, $createdDate, $updatedDate);
	$stmtu->store_result ();
	$rowu = $stmtu->fetch ();
	
	$stmts = $mysqli->prepare("SELECT standard  FROM standard WHERE standardid = '$standardid';");
	$stmts->execute ();
	$stmts->bind_result ( $standard );
	$stmts->store_result ();
	$stmts->fetch ();
	$std = $standard."th";
	
	$stmtb = $mysqli->prepare("SELECT boardname FROM board WHERE boardid = '$boardid';");
	$stmtb->execute ();
	$stmtb->bind_result ( $boardname );
	$stmtb->store_result ();
	$stmtb->fetch ();
	
	
	$userdata['userid'] = $userid;
			$userdata['userstype'] = $userstype;
			$userdata['userappid'] = $userappid;
			$userdata['name'] = $name;
			$userdata['email'] = $email;
			$userdata['password'] = $password;
			$userdata['contact'] = $contact;
			$userdata['standardid'] = $standardid;
			$userdata['standard'] = $std;
			$userdata['boardid'] = $boardid;
			$userdata['boardname'] = $boardname;
			$userdata['school'] = $school;
			$userdata['status'] = $status;
			$userdata['createdDate'] = $createdDate;
			$userdata['updatedDate'] = $updatedDate;
	
	header('Content-type: application/json');
	echo json_encode($userdata);
	
}
?>
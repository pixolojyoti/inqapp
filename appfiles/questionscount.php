<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$userid = $_GET['userid'];

	$stmt = $mysqli->prepare ( "SELECT count(*) as total FROM questions WHERE userid  = ? AND DATE(createdDate) = CURDATE()" );
	$stmt->bind_param ( 'i', $userid );
	$stmt->execute();
	$stmt->bind_result ( $total );
	$stmt->store_result ();
	$row = $stmt->fetch ();
	$cnt = 5 - $total;
	echo $cnt;

?>
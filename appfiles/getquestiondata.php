<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$questionid = $_GET['questionid'];
$userid = $_GET['userid'];
	
	$seencount = cnt($mysqli, "questionseen", "userid = '$userid' AND questionid = '$questionid'");
	if($seencount == 0){
		$stmtq = $mysqli->prepare("INSERT INTO questionseen ( userid, questionid ) VALUES (?,?)");
		$stmtq->bind_param ( "ii", $userid, $questionid );
		$stmtq->execute ();
	}
	
	$stmt = $mysqli->prepare ("SELECT questionid, userid, question, chapterid, image, createdDate, verified, imageview, video, premium, trending FROM questions WHERE questionid = '$questionid'" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $questionid, $quserid, $question, $qchapterid, $image, $qcreatedDate, $verified, $imageview, $video, $premium, $trending );
		$stmt->store_result ();
		while($row = $stmt->fetch ()) {
			$userdata = select($mysqli, "users", "userid = '$quserid'", "1");
			$qusertype = $userdata['userstype'];
			$qusername = $userdata['name'];
			
			$chapterdata = select($mysqli, "chapter", "chapterid = '$qchapterid'", "1");
			$subjectid = $chapterdata['subjectid'];
			$chapname = $chapterdata['chaptertitle'];
			
			$subjectdata = select($mysqli, "subject", "subjectid = '$subjectid'", "1");
			$subject = $subjectdata['subject'];
			
			$answerscount = cnt($mysqli, "answers", "question = '$questionid'");

			$answered = cnt($mysqli, "answers", "question = '$questionid' AND userid = '$userid'");
			if($answered != 0){
				$answered = 1;
			}
			
			$verifcnt = cnt($mysqli, "answers", "question = '$questionid' AND verified = '1'");
			
			/* if($verifcnt > 0 ){
				$verified = 1;
			}
			else{
				$verified = 0;
			} */
			
			$bookmarkedcnt = cnt($mysqli, "users_bookmark", "questionid = '$questionid' AND userid = '$userid'");
			if($bookmarkedcnt > 0 ){
				$bookmarked = 1;
			}
			else{
				$bookmarked = 0;
			}
			$time = nicetime($qcreatedDate);
			
			if($userid==$quserid && $answerscount > 0){
				$updatequestion = update($mysqli, "questions", "is_read = '1'", "questionid = $questionid");
			}

			$starcnt = cnt($mysqli, "user_starquestion", "questionid = '$questionid'");

			$viewcnt = cnt($mysqli, "questionseen", "questionid = '$questionid'");

			$userstar = 0;
			$userstar = cnt($mysqli, "user_starquestion", "questionid = '$questionid' AND userid = '$userid'");
			if($userstar>0){
				$userstar = 1;
			}

			$svstmt = $mysqli->prepare("SELECT `solutionviewed` FROM `questionseen` WHERE `questionid` = '$questionid' AND `userid` = '$userid'");
		    $svstmt->execute();
		    $svstmt->bind_result($solutionviewed);
		    $svstmt->fetch();
		    $svstmt->close();
			
			$questiondata['questionid'] = $questionid;
			$questiondata['question'] = $question;
			$questiondata['qusertype'] = $qusertype;
			$questiondata['qusername'] = $qusername;
			$questiondata['image'] = $image;
			$questiondata['time'] = $time;
			$questiondata['subject'] = $subject;
			$questiondata['qchapterid'] = $qchapterid;
			$questiondata['qchapname'] = $chapname;
			$questiondata['bookmarked'] = $bookmarked;
			$questiondata['answerscount'] = $answerscount;
			$questiondata['answered'] = $answered;
			$questiondata['verified'] = $verified;
			$questiondata['imageview'] = $imageview;
			$questiondata['video'] = $video;
			$questiondata['premium'] = $premium;
			$questiondata['trending'] = $trending;
			$questiondata['solutionviewed'] = $solutionviewed;
			$questiondata['starcount'] = $starcnt;
			$questiondata['viewcount'] = $viewcnt;
			$questiondata['star'] = $userstar;
			
		}
		header('Content-type: application/json');
		echo json_encode($questiondata);

	}
	else
	{
		echo "0";

	}

?>
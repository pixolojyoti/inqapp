<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
header('Content-type: application/x-www-form-urlencoded');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

$answer = trim($obj['answer']);
$question = $obj['question'];
$userid = $obj['userid'];
$image = $obj['image'];

$userdata = select($mysqli, "users", "userid = '$userid'", "1");
$type = $userdata['userstype'];
$quesdata = select($mysqli, "questions", "questionid = '$question'", "1");

if($userdata['userstype']==1){
	$ver = 1;
	$stmt = $mysqli->prepare("INSERT INTO answers ( userid, question, answer, image, verified, verifiedby, ausertype, createdDate ) VALUES (?,?,?,?,?,?,?,?)");
	$stmt->bind_param ( "iissiiis", $userid, $question, $answer, $image, $ver, $userid, $type, $date );
	if($stmt->execute ())
	{
		if($quesdata['verified']==0) {				
			$stmti = $mysqli->prepare("UPDATE questions SET verified = ?, verifiedby = ? WHERE questionid = ?");
			$stmti->bind_param ( "iii", $ver, $userid, $question );
			$stmti->execute ();
			echo '1';
		} else {
			echo '1';
		}
	}
	else {
		echo '0';
	}
} else {
	$ver = 0;
	$stmt = $mysqli->prepare("INSERT INTO answers ( userid, question, answer, image, verified, ausertype, createdDate ) VALUES (?,?,?,?,?,?,?)");
	$stmt->bind_param ( "iissiis", $userid, $question, $answer, $image, $ver, $type, $date );
	if($stmt->execute ()){
		echo '1';
	}
	else {
		echo '0';
	}
}
?>
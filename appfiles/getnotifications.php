<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$number = $_GET['number'];
$limit = $number + 5;
$stmt = $mysqli->prepare ( "SELECT notid, title, content, imagepath, createdDate FROM notifications ORDER BY notid DESC LIMIT $number, $limit;" );
	
	if($stmt->execute())
	{
		$stmt->bind_result ( $notid, $title, $content, $imagepath, $createdDate );
		$stmt->store_result ();
		$notificationdata = array();
		while($row = $stmt->fetch ()) {
			$time = nicetime($createdDate);
			$notificationdata[] = array(
				'title' => $title,
				'content' => $content,
				'imagepath' => $imagepath,
				'time' => $time
			);
		}
		header('Content-type: application/json');
		echo json_encode($notificationdata);

	}
	else
	{
		echo "0";

	}

?>
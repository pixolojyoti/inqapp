<?php
	header('Access-Control-Allow-Headers: Content-Type');
	header('Access-Control-Allow-Credentials: true');
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Max-Age: 86400');
	require_once("connect.php");
	require_once("function.php");

	$userid = $_GET['userid'];
	$standardid = $_GET['standardid'];
	
	
	


	if($stmt->execute())
	{
		$stmt->bind_result ( $questionid, $quserid, $question, $qchapterid, $image, $qcreatedDate, $verified, $imageview, $video, $premium );
		$stmt->store_result ();
		$questionsdata = array();

		while($row = $stmt->fetch ()) {
			$userdata = select($mysqli, "users", "userid = '$quserid'", "1");
			$qusertype = $userdata['userstype'];
			$qusername = $userdata['name'];
			
			$chapterdata = select($mysqli, "chapter", "chapterid = '$qchapterid'", "1");
			$subjectid = $chapterdata['subjectid'];
			$chapname = $chapterdata['chaptertitle'];
			
			$subjectdata = select($mysqli, "subject", "subjectid = '$subjectid'", "1");
			$subject = $subjectdata['subject'];
			
			$answered = cnt($mysqli, "answers", "question = '$questionid' AND userid = '$userid'");
			if($answered > 0 ){
				$answered = 1;
			}
			else{
				$answered = 0;
			}
			$answerscount = cnt($mysqli, "answers", "question = '$questionid'");

			/* MY STAR VALUE AND TOTAL STAR COUNT */
			$starred = cnt($mysqli, "user_starquestion", "questionid = '$questionid' AND userid = '$userid'");
			if($starred > 0 ){
				$starred = 1;
			}
			else{
				$starred = 0;
			}
			$starcount = cnt($mysqli, "user_starquestion", "questionid = '$questionid'");

			/* MY VIEWED VALUE AND TOTAL VIEW COUNT */
			$viewed = cnt($mysqli, "questionseen", "questionid = '$questionid' AND userid = '$userid'");
			if($viewed > 0 ){
				$viewed = 1;
			}
			else{
				$viewed = 0;
			}
			$viewcount = cnt($mysqli, "questionseen", "questionid = '$questionid'");

			if ($laststmt = $mysqli->prepare("SELECT `users`.`userstype` FROM `users` INNER JOIN `answers` ON `users`.`userid` = `answers`.`userid` WHERE `answers`.`question`=? ORDER BY `answers`.`createdDate` LIMIT 0,1")) {

				    /* bind parameters for markers */
				    $laststmt->bind_param("i", $questionid);

				    /* execute query */
				    $laststmt->execute();

				    /* bind result variables */
				    $laststmt->bind_result($lastseen);

				    /* fetch value */
				    $laststmt->fetch();

				    /* close statement */
				    $laststmt->close();
				}
			
			//$verifcnt = cnt($mysqli, "answers", "question = '$questionid' AND verified = '1'");
			
			/* if($verifcnt > 0 ){
				$verified = 1;
			}
			else{
				$verified = 0;
			} */
			
			$bookmarkedcnt = cnt($mysqli, "users_bookmark", "questionid = '$questionid' AND userid = '$userid'");
			if($bookmarkedcnt > 0 ){
				$bookmarked = 1;
			}
			else{
				$bookmarked = 0;
			}
			$time = nicetime($qcreatedDate);
			
			$creatdate = date('Y-m-d', strtotime($qcreatedDate));
			
			if(strtotime($creatdate)==strtotime($today)){
				$istoday = "1";
			} else{
				$istoday = "0";
			}
			
			$seencount = cnt($mysqli, "questionseen", "userid = '$userid' AND questionid = '$questionid'");
			if($seencount==0){
				$questionseen = "0";
			}else{
				$questionseen = "1";
			}
			
			$questionsdata[] = array(
				'questionid' => $questionid,
				'quserid' => $quserid,
				'question' => $question,
				'qusertype' => $qusertype,
				'qusername' => $qusername,
				'image' => $image,
				'time' => $time,
				'subject' => $subject,
				'subjectid' => $subjectid,
				'qchapterid' => $qchapterid,
				'qchaptername' => $chapname,
				'bookmarked' => $bookmarked,
				'answerscount' => $answerscount,
				'answered' => $answered,
				'starred' => $starred,
				'starcount' => $starcount,
				'viewed' => $viewed,
				'viewcount' => $viewcount,
				'verified' => $verified,
				'imageview' => $imageview,
				'video' => $video,
				'premium' => $premium,
				'istoday' => $istoday,
				'questionseen' => $questionseen,
				'lastanswered' => $lastseen
			);
			
		}
		header('Content-type: application/json');
		echo json_encode($questionsdata);

	}
	else
	{
		echo "0";

	}
?>
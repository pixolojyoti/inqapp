<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

	$photo = $_FILES['file']['name'];
	$photo_tmp = $_FILES['file']['tmp_name'];
	$filename = basename($photo);
	$ext = substr($filename, -3);
	if(is_numeric($ext)){
		$data = explode('?',$filename);
		$newimgname = renameimage($data[0]);
		if(move_uploaded_file($photo_tmp,"uploads/".$newimgname))
		{
			echo $newimgname ;
		}
		else {
			echo '0';
		}
		
	} else {
			$newimgname = renameimage($photo);
			if(move_uploaded_file($photo_tmp,"uploads/".$newimgname))
			{
				echo $newimgname ;
			}
			else {
				echo '0';
			}
	}
?>
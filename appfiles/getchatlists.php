<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

$userid = $_GET['userid'];;

$userdata = select($mysqli, "users", "userid = '$userid'", "1");
$usertype = $userdata['userstype'];
$demo = "";
if($usertype==1)
{
	$stmt = $mysqli->prepare ( "SELECT DISTINCT users.userid, users.userstype, users.name, MAX(chatmessage.createdDate) as ccd FROM users INNER JOIN chatmessage ON users.userid = chatmessage.studentid WHERE users.userstype !=1 AND chatmessage.teacherid = '$userid' GROUP BY users.userid ORDER BY ccd DESC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $cuserid, $userstype, $name, $ccd );
		$stmt->store_result ();
		$chatlists = array();
		while($row = $stmt->fetch ()) {
			
			$stmti = $mysqli->prepare ( "SELECT is_teacherread, createdDate FROM chatmessage WHERE studentid = '$cuserid' AND teacherid = '$userid' ORDER BY messageid DESC LIMIT 1" );
			$stmti->execute();
			$stmti->bind_result ( $is_teacherread, $createdDate );
			$stmti->store_result ();	
			$stmti->fetch ();
			
			$chatlists[] = array(
				'cuserid' => $cuserid,
				'cuserstype' => $userstype,
				'cname' => $name,
				'isread' => $is_teacherread,
				'lastmsgtime' => date('h:i a',strtotime($createdDate))
			);
		}
		header('Content-type: application/json');
		echo json_encode($chatlists);

	}
	else
	{
		echo "0";

	}
}
else
{
	
	$stmt = $mysqli->prepare ( "SELECT DISTINCT users.userid, users.userstype, users.name, MAX(chatmessage.createdDate) as ccd FROM users INNER JOIN chatmessage ON users.userid = chatmessage.teacherid WHERE users.userstype = '1' AND chatmessage.studentid = '$userid' GROUP BY users.userid ORDER BY ccd DESC" );
	if($stmt->execute())
	{
		$stmt->bind_result ( $cuserid, $userstype, $name, $ccd );
		$stmt->store_result ();
		$chatlists = array();
		$chatuserid = array();
		while($row = $stmt->fetch ()) {
			$stmti = $mysqli->prepare ( "SELECT is_studentread, createdDate FROM chatmessage WHERE studentid = '$userid' AND teacherid = '$cuserid' ORDER BY messageid DESC LIMIT 1" );
			$stmti->execute();
			$stmti->bind_result ( $is_studentread, $createdDate );
			$stmti->store_result ();	
			$stmti->fetch ();
			
			array_push($chatuserid , $cuserid);
			
			$chatlists[] = array(
				'cuserid' => $cuserid,
				'cuserstype' => $userstype,
				'cname' => $name,
				'isread' => $is_studentread,
				'lastmsgtime' => date('h:i a',strtotime($createdDate))
			);
		}
		
		$str = "SELECT DISTINCT userid, userstype, name FROM users WHERE userstype = '1' ";
		foreach($chatuserid as $id){
			if(!empty($id)){
				$str.="AND userid != '$id'";
			}
		}
		$str.= " ORDER BY name ASC";
		$stmti = $mysqli->prepare ( $str );
		$stmti->execute();
				$stmti->bind_result ( $cuserid, $userstype, $name );
				$stmti->store_result ();
				$cnt = $stmti->num_rows;
				if( $cnt != sizeof($chatuserid)) {
					while($rowi = $stmti->fetch ()) {
						
						$chatlists[] = array(
							'cuserid' => $cuserid,
							'cuserstype' => $userstype,
							'cname' => $name,
							'isread' => '1',
							'lastmsgtime' => $demo
						);
					}
				}
		header('Content-type: application/json');
		echo json_encode($chatlists);

	}
	else
	{
		echo "0";

	}
}

?>
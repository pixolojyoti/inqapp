<?php
header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$questionid = $_GET['questionid'];
$userid = $_GET['userid'];
$ver = "1";

	$stmt = $mysqli->prepare("UPDATE questions SET verified = ?, verifiedby = ? WHERE questionid = ?");
	$stmt->bind_param ( "iii", $ver, $userid, $questionid );
	if($stmt->execute ()){
		echo '1';
	}
	else {
		echo '0';
	}
?>
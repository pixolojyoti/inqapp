<?php

header('Access-Control-Allow-Headers: Content-Type');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Max-Age: 86400');
require_once("connect.php");
require_once("function.php");

date_default_timezone_set("Asia/Kolkata");
$date = date('Y-m-d H:i:s');

$json = file_get_contents('php://input');
$obj = json_decode($json, true);

$image= $obj['image'];

 $image_parts = explode(";base64,", $image);
 $image_type_aux = explode("image/", $image_parts[0]);
 $image_type = $image_type_aux[1];
 $image_base64 = base64_decode($image_parts[1]);

$name = md5(date('Y-m-d H:i:s:u')).".jpg";

$path = "uploads/".$name;

file_put_contents($path , $image_base64);

echo $name;




?>
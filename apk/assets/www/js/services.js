//var adminurl = "http://inqapp.crextal.com/appfiles/";
//var adminurl = "http://learnwithinq.com/appfilesv2/";
var adminurl = "http://www.learnwithinq.com/appfilesv3/";
var adminurl2 = "http://www.learnwithinq.com/appfilesv2/";
//var adminurl = "http://localhost/inqv2/appfiles/";
var myservices = angular.module('myservices', [])


    .factory('MyServices', function ($http, $location, $ionicPopup) {


        return {
            login: function (contact, pasword) {
                console.log(contact);
                return $http.post(adminurl + "login.php", {
                    data: {
                        contact: contact,
                        password: pasword
                    }

                });
            },
            login2: function (contact, pasword) {
                console.log(contact);
                console.log(pasword);
                var Indata = {
                    contact: contact,
                    password: pasword
                };
                return $http({
                    url: "login.php",
                    method: "POST",
                    params: Indata
                });
            },
            register: function (registerdata) {

                /*var Indata = {
                    'name': registerdata.name,
                    'contact': registerdata.contact,
                    'email': registerdata.email,
                    'password': registerdata.password,
                    'standard': registerdata.standard,
                    'board': registerdata.board,
                    'school': registerdata.school
                };
                return $http({
                    url: adminurl+"register.php",
                    method: "POST",
                    params: Indata
                });*/
                console.log(registerdata);
                return $http.get(adminurl + "register.php", {

                    params: {
                        'name': registerdata.name,
                        'contact': registerdata.contact,
                        'email': registerdata.email,
                        'password': registerdata.password,
                        'standard': registerdata.standard,
                        'board': registerdata.board,
                        'school': registerdata.school,
                        'promocode': registerdata.promocode
                    }
                })
            },
            loginget: function (contact, pasword) {
                return $http.get(adminurl + "login.php", {
                    params: {
                        contact: contact,
                        password: pasword
                    }
                });
            },
            adddevice: function (userid, deviceid) {
                return $http.get(adminurl + "adddevice.php", {
                    params: {
                        userid: userid,
                        deviceid: deviceid
                    }
                });
            },
            getusersubjects: function (stdid) {
                return $http.get(adminurl + "getsubjects.php", {
                    params: {
                        'standardid': stdid
                    }
                })
            },
            getstandards: function () {
                return $http.get(adminurl + 'getallstandards.php', {
                    params: {}
                })
            },
            getboards: function () {
                return $http.get(adminurl + 'getallboards.php', {
                    params: {}
                })
            },
            bookmarkques: function (uid, qid) {
                return $http.get(adminurl + 'bookmarkques.php', {
                    params: {
                        'userid': uid,
                        'questionid': qid
                    }
                })
            },
            verifyanswer: function (uid, aid) {
                return $http.get(adminurl + 'verifiedanswer.php', {
                    params: {
                        'userid': uid,
                        'answerid': aid
                    }
                })
            },
            verifyquestion: function (uid, qid) {
                return $http.get(adminurl + 'verifyquestion.php', {
                    params: {
                        'userid': uid,
                        'questionid': qid
                    }
                })
            },
            staranswer: function (uid, aid) {
                return $http.get(adminurl + 'staranswer.php', {
                    params: {
                        'userid': uid,
                        'answerid': aid
                    }
                })
            },
            starquestion: function (uid, qid) {
                return $http.get(adminurl + 'starquestion.php', {
                    params: {
                        'userid': uid,
                        'questionid': qid
                    }
                })
            },
            getviewedquestionusers: function (qid) {
                return $http.get(adminurl + 'getviewedquestionusers.php', {
                    params: {
                        'questionid': qid
                    }
                })
            },
            getstarquestionusers: function (qid) {
                return $http.get(adminurl + 'getstarquestionusers.php', {
                    params: {
                        'questionid': qid
                    }
                })
            },
            getstaranswerusers: function (aid) {
                return $http.get(adminurl + 'getstaranswerusers.php', {
                    params: {
                        'answerid': aid
                    }
                })
            },
            quesleft: function (uid) {
                //var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'questionscount.php', {
                    params: {
                        'userid': uid
                    }
                })
            },
            askquestion: function (uid, ques, cid, img, imgview, premium, trending, video) {
                return $http.post(adminurl + 'addquestions.php', {
                    'userid': uid,
                    'chapterid': cid,
                    'question': ques,
                    'image': img,
                    'imageview': imgview,
                    'premium': premium,
                    'trending': trending,
                    'video': video
                });
            },
            answerques: function (uid, answer, qid, img, canverify) {
                console.log(qid);
                return $http.post(adminurl + 'addanswer.php', {
                    'userid': uid,
                    'question': qid,
                    'answer': answer,
                    'image': img,
                    'canverify': canverify
                });
            },
            gettrendingquestions: function (standard, user) {
                return $http.get(adminurl2 + 'gettrending.php', {
                    params: {
                        'standardid': standard,
                        'userid': user
                    }
                });
            },
            getquestions: function (filters, s) {

                if (s == undefined) {
                    s = '';
                };
                console.log(s);

                var chapters = "";
                if (filters.chapters.length > 0) {
                    for (var cs = 0; cs < filters.chapters.length; cs++) {
                        chapters += filters.chapters[cs];
                        if (cs != filters.chapters.length - 1) {
                            chapters += ',';
                        };
                    };
                };

                console.log(filters);

                return $http.get(adminurl2 + 'getquestions.php', {
                    params: {
                        'userid': filters.userid,
                        'standardid': filters.standardid,
                        'subjectid': filters.subjectid,
                        'chapters': chapters,
                        'number': filters.number,
                        'limit': filters.limit,
                        'filters': filters.filters,
                        'search': s
                    }
                });
            },
            getquestionsearch: function (filters, s) {

                var chapters = "";
                if (filters.chapters.length > 0) {
                    for (var cs = 0; cs < filters.chapters.length; cs++) {
                        chapters += filters.chapters[cs];
                        if (cs != filters.chapters.length - 1) {
                            chapters += ',';
                        };
                    };
                };

                return $http.get(adminurl + 'getquestionsearch.php', {
                    params: {
                        'userid': filters.userid,
                        'standardid': filters.standardid,
                        'subjectid': filters.subjectid,
                        'chapters': chapters,
                        'number': filters.number,
                        'limit': filters.limit,
                        'filters': filters.filters,
                        'search': s
                    }
                });
            },
            getquestiondata: function (qid) {
                var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'getquestiondata.php', {
                    params: {
                        'questionid': qid,
                        'userid': user
                    }
                });
            },
            viewedsolution: function (qid) {
                var userid = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'viewedsolution.php', {
                    params: {
                        'questionid': qid,
                        'userid': userid
                    }
                });
            },
            getpending: function (sid) {
                return $http.get(adminurl + 'pending.php', {
                    params: {
                        'standardid': sid
                    }
                });
            },
            getpendingstatus: function (sid) {
                return $http.get(adminurl + 'getpendingstatus.php', {
                    params: {
                        'standardid': sid
                    }
                });
            },
            getanswerdata: function (aid) {
                return $http.get(adminurl + 'getanswerdata.php', {
                    params: {
                        'answerid': aid
                    }
                });
            },
            getanswers: function (qid) {
                var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'getanswers.php', {
                    params: {
                        'questionid': qid,
                        'userid': user
                    }
                });
            },
            editprofile: function (user) {
                console.log(user);
                return $http.get(adminurl + 'editprofile.php', {
                    params: {
                        'userid': user.userid,
                        'name': user.name,
                        'contact': user.contact,
                        'email': user.email,
                        'school': user.school
                    }
                });
            },
            changepassword: function (pass) {
                console.log(pass);
                var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'changepassword.php', {
                    params: {
                        'userid': user,
                        'password': pass
                    }
                });
            },
            forgotpassword: function (contact, pass) {
                return $http.get(adminurl + 'forgotpassword.php', {
                    params: {
                        'contact': contact,
                        'password': pass
                    }
                });
            },
            editanswer: function (aid, ans, img) {
                return $http.post(adminurl + 'editanswer.php', {
                    'answerid': aid,
                    'answer': ans,
                    'image': img
                });
            },
            editquestion: function (qid, cid, ques, img, imgview, premium, trending, video) {
                return $http.post(adminurl + 'editquestion.php', {
                    'questionid': qid,
                    'chapterid': cid,
                    'question': ques,
                    'image': img,
                    'imageview': imgview,
                    'premium': premium,
                    'trending': trending,
                    'video': video
                });
            },
            deleteanswer: function (aid) {
                return $http.get(adminurl + 'deleteanswer.php', {
                    params: {
                        'answerid': aid
                    }
                });
            },
            deletequestion: function (qid) {
                return $http.get(adminurl + 'deletequestion.php', {
                    params: {
                        'questionid': qid
                    }
                });
            },

            /*CHAT*/
            getchatlist: function (uid) { /*getchatlist.php*/
                return $http.get(adminurl + 'getchatlists.php', {
                    params: {
                        'userid': uid
                    }
                });
            },
            getmessages: function (tid, sid) {
                var userid = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'getchatmessages.php', {
                    params: {
                        'userid': userid,
                        'teacherid': tid,
                        'studentid': sid
                    }
                });
            },
            sendmessage: function (msg) {
                return $http.post(adminurl + 'addmessages.php', {
                    'message': msg.message,
                    'image': msg.image,
                    'teacherid': msg.teacherid,
                    'studentid': msg.studentid,
                    'senderid': msg.senderid
                });
            },
            sendsms: function (contact, msg) {
                return $http.get('http://bulksms.mysmsmantra.com:8080/WebSMS/SMSAPI.jsp', {
                    params: {
                        'username': 'INQClasses',
                        'password': '149097547',
                        'sendername': 'SUNALI',
                        'mobileno': '91' + contact,
                        'message': msg
                    }
                });
            },
            checkfornewmessages: function () {
                var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'checknewmessages.php', {
                    params: {
                        'userid': user
                    }
                });
            },
            checkanswered: function () {

                if ($.jStorage.get("user")) {
                    var standard = $.jStorage.get("user").standardid;
                };

                return $http.get(adminurl + 'checkanswered.php', {
                    params: {
                        'standardid': standard
                    }
                });
            },
            getprofilecounts: function () {
                var user = $.jStorage.get("user").userid;
                return $http.get(adminurl + 'quesanscount.php', {
                    params: {
                        'userid': user
                    }
                });
            },
            gettopasked: function () {
                var user = $.jStorage.get("user").standardid;
                return $http.get(adminurl + 'questiontopten.php', {
                    params: {
                        'standardid': user
                    }
                });
            },
            gettopanswered: function () {
                var user = $.jStorage.get("user").standardid;
                return $http.get(adminurl + 'answertopten.php', {
                    params: {
                        'standardid': user
                    }
                });
            },
            getnotifications: function (num) {
                return $http.get(adminurl + 'getnotifications.php', {
                    params: {
                        'number': num
                    }
                });
            },
            showToast: function (msg) {
                console.log(msg);
                window.plugins.toast.showShortBottom(msg, function (a) {
                    console.log('toast success: ' + a)
                }, function (b) {
                    alert('toast error: ' + b)
                });
            },
            viewanalyse: function (view) {
                if (window.location.hostname != "localhost") {
                    if (typeof analytics !== undefined) {
                        analytics.trackView(view);
                    }
                }
            },
            useranalyse: function (id) {
                if (window.location.hostname != "localhost") {
                    if (typeof analytics !== undefined) {
                        analytics.setUserId(id)
                    }
                }
            },

            /*PREMIUM QUESTION COUNT*/
            checkpremiumquestions: function (userid) {
                return $http.get(adminurl + 'quesanscount.php', {
                    params: {
                        'userid': userid
                    }
                });
            },

            decreasepremiumquestioncount: function (userid) {

                return $http.get(adminurl + 'subtractquestion.php', {
                    params: {
                        'userid': userid
                    }
                });


            },


            /*CHANGE NOTIFICATION STATUS*/
            getnotificationsstatus: function (userid, deviceid) {

                return $http.get(adminurl + 'getnotificationstatus.php', {
                    params: {
                        'userid': userid,
                        'deviceid': deviceid
                    }
                });

            },
            sendnotificationupdate: function (userid, deviceid) {


                return $http.get(adminurl + 'togglenotificationstatus.php', {
                    params: {
                        'userid': userid,
                        'deviceid': deviceid
                    }
                });
            },
            /*CHECK IF CONTACT EXISTS*/

            checkcontactexist: function (contact) {
                return $http.get(adminurl + 'checkcontactexist.php', {
                    params: {
                        'contact': contact
                    }
                });
            },

            checkcouponcode: function (deviceid, promouserid) {




                return $http.get(adminurl + 'validatepromocode.php', {
                    params: {
                        'deviceid': deviceid,
                        'promocode': promouserid,
                    }
                });
            },

            logout: function (deviceid, userid) {

                return $http.get(adminurl + 'logout.php', {
                    params: {
                        'userid': userid,
                        'deviceid': deviceid
                    }
                });


            },

            confirmationpopupforpaidquestions: function (msg, viewname) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'No questions left',
                    template: msg,
                    cssClass: 'confirmpopup bgstyle'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        $location.path("/app/" + viewname);
                    } else {
                        //                        console.log('You are not sure');
                    }
                });



            },
            gettransactionstatus: function (userid, questioncount) {


                return $http.get(adminurl + 'gettransactionstatus.php', {
                    params: {
                        'userid': userid,
                        'questioncount': questioncount
                    }
                });



            },

            getquestioncost: function () {
                return $http.get(adminurl + 'getquestioncost.php', {
                    params: {

                    }
                });

            },

            purchaseavailable: function () {
                return $http.get(adminurl + 'purchaseavailable.php', {
                    params: {

                    }
                });


            },







        }

    });

// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

function handleOpenURL(url) {
    setTimeout(function () {
        var location = url.split("//")[1];
        window.location = "index.html#/app/" + location;
    }, 0);
}

angular.module('starter', ['ionic', 'starter.controllers', 'myservices', 'ngCordova'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)

            console.log('location path ', window.location)

            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

            /*COMMENT ANALYTICS CODE DURING DEVELOPMENT*/
            if (window.location.hostname != "localhost") {
                if (typeof analytics !== 'undefined') {
                    analytics.startTrackerWithId("UA-89647383-1");

                    var anuser = $.jStorage.get("user");
                    if (!user || user == {}) {

                    } else {
                        analytics.setUserId(anuser.userid)
                    };
                } else {
                    console.log("Google Analytics Unavailable");
                };



                // Enable to debug issues.
                // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

                var notificationOpenedCallback = function (result) {
                    console.log(result);
                    console.log(window.location);
                    if (!result.notification.isAppInFocus) {
                        var data = result.notification.payload.additionalData;
                        if (data && data.targetUrl) {
                            var state = $injector.get($state);
                            state.go(data.targetUrl);
                        };

                        window.location = "index.html#/app/" + data.url + "/:" + data.urlid;
                    }

                };

                /*COMMENT NOTIFICATION INITIALIZATION DURING DEVELOPMENT*/
                window.plugins.OneSignal
                    .startInit("e236a9bf-6e68-4844-a438-ead30f932026")
                    .handleNotificationOpened(notificationOpenedCallback)
                    .endInit();
                /*-----------------------------------*/

//                OneSignal.push(function () {
//                    OneSignal.on('subscriptionChange', function (isSubscribed) {
//                        if (isSubscribed) {
//                            // The user is subscribed
//                            //   Either the user subscribed for the first time
//                            //   Or the user was subscribed -> unsubscribed -> subscribed
//                            OneSignal.getUserId(function (userId) {
//                                // Make a POST call to your server with the user ID
//                            });
//                        }
//                    });
//                });


            };

            /*--------------------------*/





            // Call syncHashedEmail anywhere in your app if you have the user's email.
            // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
            // window.plugins.OneSignal.syncHashedEmail(userEmail);

        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })
            .state('app.feedback', {
                url: '/feedback',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/feedback.html',
                        controller: 'feedbackCtrl'
                    }
                }
            })
            .state('app.playlists', {
                url: '/questions',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/playlists.html',
                        controller: 'PlaylistsCtrl'
                    }
                }
            })

            .state('app.single', {
                url: '/questions/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/playlist.html',
                        controller: 'PlaylistCtrl'
                    }
                }
            })
            .state('app.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/profile.html',
                        controller: 'profileCtrl'
                    }
                }
            })
            .state('app.paq', {
                url: '/paq',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/paq.html',
                        controller: 'paqCtrl'
                    }
                }
            })
            .state('app.bookmarks', {
                url: '/bookmarks',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/bookmarks.html',
                        controller: 'bookmarksCtrl'
                    }
                }
            })
            .state('app.trending', {
                url: '/trending',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/playlists.html',
                        controller: 'trendingCtrl'
                    }
                }
            })
            .state('app.pending', {
                url: '/pending',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/pending.html',
                        controller: 'pendingCtrl'
                    }
                }
            })
            .state('app.ask', {
                url: '/ask',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/ask.html',
                        controller: 'askCtrl'
                    }
                }
            })
            .state('app.edit', {
                url: '/edit',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/edit.html',
                        controller: 'editCtrl'
                    }
                }
            })
            .state('app.filter', {
                url: '/filter',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/filter.html',
                        controller: 'filterCtrl'
                    }
                }
            })
            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'
                    }
                }
            })
            .state('app.otp', {
                url: '/otp',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/otp.html',
                        controller: 'otpCtrl'
                    }
                }
            })
            .state('app.standard', {
                url: '/standard',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/standard.html',
                        controller: 'standardCtrl'
                    }
                }
            })
            /*.state('app.chatlist', {
                url: '/chatlist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chatlist.html',
                        controller: 'chatlistCtrl'
                    }
                }
            })
            .state('app.chat', {
                url: '/chat',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/chat.html',
                        controller: 'chatCtrl'
                    }
                }
            })*/
            .state('app.intro', {
                url: '/intro',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/intro.html',
                        controller: 'introCtrl'
                    }
                }
            }).state('app.leaderboard', {
                url: '/leaderboard',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/leaderboard.html',
                        controller: 'leaderboardCtrl'
                    }
                }
            })
            .state('app.notifications', {
                url: '/notifications',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/notifications.html',
                        controller: 'notificationsCtrl'
                    }
                }
            })

            .state('app.payment', {
                url: '/payment',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/payment.html',
                        controller: 'paymentCtrl'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/playlists');


        $ionicConfigProvider.navBar.alignTitle('center');

        /**/

    })
    .filter('imagepath', function () {
        return function (input) {
            return "http://learnwithinq.com/appfiles/uploads/" + input;
        };
    })
    .filter('videopath', function () {
        return function (input) {
            return "https://www.youtube.com/embed/" + input;
        };
    })
    .filter('trusted', ['$sce', function ($sce) {
        return function (url) {
            if (url != undefined) {
                if (url.indexOf("v=") != -1) {
                    var video_id = url.split('v=')[1];
                    if (video_id.indexOf('&') != -1) {
                        video_id = video_id.split('&')[0];
                    };
                } else {
                    if (url.indexOf("youtu.be") != -1) {
                        var video_id = url.split("be/")[1];
                    }
                };

                return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + video_id);
            }
        };
    }])
    .filter('youtubetrusted', ['$sce', function ($sce) {
        return function (url) {

            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            var video_id = (match && match[7].length == 11) ? match[7] : false;

            return $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + video_id);
        }
    }])
    .filter('capitalize', function () {
        return function (input, all) {
            var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
            return (!!input) ? input.replace(reg, function (txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }) : '';
        }
    })
    .filter('allcapitalize', function () {
        return function (input) {
            return (!!input) ? input.split(' ').map(function (wrd) {
                return wrd.charAt(0).toUpperCase() + wrd.substr(1).toLowerCase();
            }).join(' ') : '';
        }
    })
    .filter('truefalse', function () {
        return function (val) {
            return val == '1' ? true : false;
        }
    })
    .filter('negateint', function () {
        return function (val) {
            return val == '1' ? 0 : 1;
        }
    })
    .filter('useridfrompromocode', function () {
        return function (promocode) {
            //operations to get userid
            return promocode.substr(3);
        }
    })
    .filter('useridtopromocode', function () {
        return function (userid, username) {
            //operations to get promocode
            return 'SC' + (username.substr(0, 1)).toUpperCase() + userid;
        }
    })
    .filter('getdeviceid', function () {
        return function () {
            //get onesignal id
            var deviceid = '';
            //            var deviceid = '1fe6bbf7-5334-465e-a04b-4fb4aa3919c4';
            return window.plugins.OneSignal.getIds(function (ids) {
                deviceid = JSON.stringify(ids['userId']);
                return deviceid;
            });

        }
    })
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    })
    .directive('imageonload', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.bind('load', function () {
                    alert('image is loaded');
                });
                element.bind('error', function () {
                    alert('image could not be loaded');
                });
            }
        };
    })
    .run(function ($rootScope, $ionicPlatform, $ionicHistory, $location) {
        $ionicPlatform.registerBackButtonAction(function (e) {

            var loop1 = false;

            if ($rootScope.backButtonPressedOnceToExit) {
                ionic.Platform.exitApp();
            } else {
                if ($location.path() == '/app/login') {
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    window.plugins.toast.showShortCenter(
                        "Press back button again to exit",
                        function (a) {},
                        function (b) {}
                    );
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/playlists') {
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    window.plugins.toast.showShortCenter(
                        "Press back button again to exit",
                        function (a) {},
                        function (b) {}
                    );
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
                if ($location.path() == '/app/intro') {
                    loop1 = true;
                    $rootScope.backButtonPressedOnceToExit = true;
                    window.plugins.toast.showShortCenter(
                        "Press back button again to exit",
                        function (a) {},
                        function (b) {}
                    );
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);
                };
            };

            if (loop1 == false) {
                if ($rootScope.backButtonPressedOnceToExit) {
                    ionic.Platform.exitApp();
                } else if ($ionicHistory.backView()) {
                    $ionicHistory.goBack();
                } else {
                    window.location = "index.html#/app/playlists"
                    $rootScope.backButtonPressedOnceToExit = true;
                    window.plugins.toast.showShortCenter(
                        "Press back button again to exit",
                        function (a) {},
                        function (b) {}
                    );
                    setTimeout(function () {
                        $rootScope.backButtonPressedOnceToExit = false;
                    }, 2000);

                }
            };

            e.preventDefault();
            return false;
        }, 101);

    });
